package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.*;

@Path("/cars")
public class CarManager {
// 	Empty constructor to access the manager in other classes when needed	
	public CarManager() {
		
	}
//	Creates a sorted List using our created CSDLList which uses CarComparator as a parameter and Car as our data type

	private final static SortedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());                               

//	Gets all cars in the list
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] tmp = new Car[cList.size()];
		for(int i =0; i< cList.size(); i++) {
			tmp[i] = cList.get(i);
		}
		return tmp;
	}   
	

//	Gets cars by ID: since ID has to be unique, iterates through all days of the week until it finds its param ID
//	Throws a NFException if ID does not exist
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCar(@PathParam("id") long id){
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarId() == id) {
				  return cList.get(i);
			  }
		  }
		  
	      throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	    
	  }  
//		Returns an array with the cars whose brands matches the param carBrand on the list. 

	  @GET
	  @Path("/brand/{carBrand}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getCarsByBrand(@PathParam("carBrand") String carBrand){
		  int counterSize = 0;
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarBrand().equals(carBrand)) {
				 counterSize++;
			  }
		  }
		  Car[] tmp = new Car[counterSize];
		  counterSize = 0;
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarBrand().equals(carBrand)) {
				 tmp[counterSize] = cList.get(i);
				 counterSize++;
			  }
		  }
		  
	    return tmp;
	  }  
	  
//		Returns an array with the cars whose year matches the param carYear on the list. 

	  @GET
	  @Path("/year/{carYear}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getCarsByYear(@PathParam("carYear") Integer carYear){
		  int counterSize = 0;
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarYear().equals(carYear)) {
				 counterSize++;
			  }
		  }
		  Car[] tmp = new Car[counterSize];
		  counterSize = 0;
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarYear().equals(carYear)) {
				 tmp[counterSize] = cList.get(i);
				 counterSize++;
			  }
		  }
		  
	    return tmp;
	  }  
//		Adds a new car to the sorted list
//		Before adding, verifies that the ID of the new car doesn't match an existing ID; if it does, 
//		gives NOT ACCEPTABLE response status
	  	@POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCar(Car car){
	  		Car tmp = new Car();
			  for(int i = 0; i<cList.size(); i++) {
				  tmp = cList.get(i);
				if(tmp.getCarId() == car.getCarId())
					return Response.status(Response.Status.NOT_ACCEPTABLE).build();     
					}
	      cList.add(car);
	      return Response.status(201).build();
	    }  
	  	

//		Uses the ID of the parameter car and compares it within the existing ones in the list. 
//		When matched, removes the current car on the list and adds the updated one to it
//		However, if ID in question isn't found on the list, it returns a response NOT FOUND
	  	

	    @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCar(Car car){
	    	for(int i = 0; i<cList.size(); i++) {
	    		if(car.getCarId() == cList.get(i).getCarId()) {
	    			cList.remove(i);
	    			cList.add(car);
	    	        return Response.status(Response.Status.OK).build();

	    		}
	    	}
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      }
//		Iterates through the lists until the car with the ID in the parameter is found, then deletes it. Otherwise, returns NOT FOUND	    
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){
	    	for(int i = 0; i<cList.size();i++) {
	    		if(id == cList.get(i).getCarId()) {
	    			cList.remove(i);
	    	        return Response.status(Response.Status.OK).build();

	    	}
	      }
	        return Response.status(Response.Status.NOT_FOUND).build();      
	    }  
	   
}
