package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarUnitComparator<E> implements Comparator<E> {

	@Override
	public int compare(E o1, E o2) {
		CarUnit unit1 = (CarUnit) o1;
		CarUnit unit2 = (CarUnit) o2;
		
		return unit1.getVin().compareTo(unit2.getVin());
	}

}
