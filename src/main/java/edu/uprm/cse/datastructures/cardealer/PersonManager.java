package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.*;

@Path("/person")
public class PersonManager {	
// 	Empty constructor to access the manager in other classes when needed	
	public PersonManager() {
		
	}
//	Creates a sorted List using our created CSDLList which uses PersonComparator as a parameter and Person as our data type
		private final static SortedList<Person> cList = new CircularSortedDoublyLinkedList<Person>(new PersonComparator<Person>());                               

//		Gets all carUnits in the list
		@GET
		@Path("")
		@Produces(MediaType.APPLICATION_JSON)
		public Person[] getAllPersons() {
			Person[] tmp = new Person[cList.size()];
			for(int i =0; i< cList.size(); i++) {
				tmp[i] = cList.get(i);
			}
			return tmp;
		}   
		
//		Gets car by ID
//		Throws a NFException if ID does not exist
		  @GET
		  @Path("/{id}")
		  @Produces(MediaType.APPLICATION_JSON)
		  public Person getPerson(@PathParam("id") long id){
			  for(int i = 0; i<cList.size();i++) {
				  if(cList.get(i).getPersonId() == id) {
					  return cList.get(i);
				  }
			  }
			  
		      throw new NotFoundException(new JsonError("Error", "Person " + id + " not found"));
		    
		  }  
//			Returns an array with the persons whose last name matches the param lastName on the list. 
		  @GET
		  @Path("/lastname/{lastName}")
		  @Produces(MediaType.APPLICATION_JSON)
		  public Person[] getPersonByLastName(@PathParam("lastName") String lastName){
			  int counterSize = 0;
			  for(int i = 0; i<cList.size();i++) {
				  if(cList.get(i).getLastName().equals(lastName)) {
					 counterSize++;
				  }
			  }
			  Person[] tmp = new Person[counterSize];
			  counterSize = 0;
			  
			  for(int i = 0; i<cList.size();i++) {
				  if(cList.get(i).getLastName().equals(lastName)) {
					 tmp[counterSize] = cList.get(i);
					 counterSize++;
				  }
			  }
			  
		    return tmp;
		  }  
//			Adds a new car to the sorted list
//			Before adding, verifies that the ID of the new person OR the phone number doesnt match the ones of any persons
//			already on the list. Otherwise, gives NOT ACCEPTABLE response status	  
		  	@POST
		    @Path("/add")
		    @Produces(MediaType.APPLICATION_JSON)
		    public Response addPerson(Person person){
		  		Person tmp = new Person();
				  for(int i = 0; i<cList.size(); i++) {
					  tmp = cList.get(i);
					if(tmp.getPersonId() == person.getPersonId() || (tmp.getPhone() == person.getPhone()))
						return Response.status(Response.Status.NOT_ACCEPTABLE).build();     
						}
		      cList.add(person);
		      return Response.status(201).build();
		    }  
		  	

		  	
//			Uses the ID of the parameter person and compares it within the existing ones in the list. 
//			When matched, removes the current person on the list and adds the updated one to it
//			However, if ID in question isn't found on the list, it returns a response NOT FOUND

		    @PUT
		    @Path("/{id}/update")
		    @Produces(MediaType.APPLICATION_JSON)
		    public Response updatePerson(Person person){
		    	for(int i = 0; i<cList.size(); i++) {
		    		if(person.getPersonId() == cList.get(i).getPersonId()) {
		    			cList.remove(i);
		    			cList.add(person);
		    	        return Response.status(Response.Status.OK).build();

		    		}
		    	}
		        return Response.status(Response.Status.NOT_FOUND).build();      
		      }
		    
		    @DELETE
		    @Path("/{id}/delete")
		    public Response deletePerson(@PathParam("id") long id){
		    	for(int i = 0; i<cList.size();i++) {
		    		if(id == cList.get(i).getPersonId()) {
		    			cList.remove(i);
		    	        return Response.status(Response.Status.OK).build();

		    	}
		      }
		        return Response.status(Response.Status.NOT_FOUND).build();      
		    }  

}
