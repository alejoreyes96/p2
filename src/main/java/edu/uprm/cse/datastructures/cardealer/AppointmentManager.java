package edu.uprm.cse.datastructures.cardealer;
import java.util.Iterator;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;       


@Path("/appointment")
public class AppointmentManager {
	
// 	Empty constructor to access the manager in other classes when needed	
	public AppointmentManager() {
		
	}
// 	Filling a positional List array with 5 linked positional Lists: one for each day of the week from Monday through Friday using Appointment as our data type
	private final static LinkedPositionalList<Appointment> lList = new LinkedPositionalList<Appointment>();
	private final static LinkedPositionalList<Appointment> mList = new LinkedPositionalList<Appointment>();
	private final static LinkedPositionalList<Appointment> wList = new LinkedPositionalList<Appointment>();
	private final static LinkedPositionalList<Appointment> jList = new LinkedPositionalList<Appointment>();
	private final static LinkedPositionalList<Appointment> vList = new LinkedPositionalList<Appointment>();
	private final static LinkedPositionalList<Appointment>[] appList = new LinkedPositionalList[] {lList, mList, wList, jList, vList};
	
		
//	Gets all appointments in all of the week
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[][] getAllAppointments() {
		Appointment[][] tmp = new Appointment[appList.length][];
		for(int i = 0; i<appList.length; i++) {
			Iterator<Appointment> itr = appList[i].iterator();
			tmp[i] = new Appointment[appList[i].size()];
			for(int j = 0; j<appList[i].size(); j++) {
				tmp[i][j] = itr.next();
			}
		}
		return tmp;
		
	}   
	
//	Gets apointments by ID: since ID has to be unique, iterates through all days of the week until it finds its param ID
//	Throws a NFException if ID does not exist
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Appointment getAppointment(@PathParam("id") long id){
		  Appointment tmp = new Appointment();
		  for(int i = 0; i<appList.length; i++) {
				Iterator<Appointment> itr = appList[i].iterator();
				for(int j = 0; j<appList[i].size(); j++) {
					tmp = itr.next();
					if(tmp.getAppointmentId() == id) {
						return tmp;
					}
				}
			}
		  
	      throw new NotFoundException(new JsonError("Error", "Appointment " + id + " not found"));
	    
	  }  

//		Enables search of appointments by day. Uses a String as parameter (i.e Monday) and gives out all appointments for that day
//		Defaults to Monday if param/string given is incorrect
	  @GET
	  @Path("/day/{day}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Appointment[] searchAppointmentByDay(@PathParam("day") String day){
		  
		  int dayId = findDay(day);
		  Appointment[] tmp= new Appointment[appList[dayId].size()];
		  Iterator<Appointment> itr = appList[dayId].iterator();
		  for(int j = 0; j<appList[dayId].size(); j++) {
				tmp[j] = itr.next();	
			}
				
			return tmp;
		}

//		Adds a new appointment using a number as a parameter (i.e Monday = 0, Tuesday = 1, and so on)
//		Before adding, it checks if the carUnitId given out in the appointment exists by using the CarUnitManager getById method
//		Even though the try/catch expression is not necessary, I keep it for code reading simplicity (the same exception would be called anyways).
//		Lastly, it checks that the ID of the new appointment isn't already on the list, giving a NOT ACCEPTABLE response status otherwise	  	
	  @POST
	    @Path("/add/{day}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addAppointment(@PathParam("day") int day, Appointment app){
	  		CarUnitManager carUnit = new CarUnitManager();
	  		Appointment tmp = new Appointment();
	  		try {
	  			carUnit.getCarUnit(app.getCarUnitId());
	  		}
	  		catch(NotFoundException e) {
	  	      throw new NotFoundException(new JsonError("Error", "Car Unit " + app.getCarUnitId()+ " not found"));
	  		}
			for(int i = 0; i<appList.length; i++) {
				Iterator<Appointment> itr = appList[i].iterator();
				for(int j = 0; j<appList[i].size(); j++) {
					tmp = itr.next();
					if(tmp.getAppointmentId() == app.getAppointmentId())
						return Response.status(Response.Status.NOT_ACCEPTABLE).build();     
					}
				}
			 appList[day].addLast(app);
			 return Response.status(201).build();
	    }  
	  	

	  	
//		Uses the ID of the parameter appointment and compares it within the existing ones in the list. 
//		When matched, it verifies all of the fields inside the appointment and changes them accordingly and returns
//		However, if ID in question isn't found on the list, it returns a response NOT FOUND
	    @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateAppointment(Appointment app){
	    	 Appointment tmp = new Appointment();
			  for(int i = 0; i<appList.length; i++) {
					Iterator<Appointment> itr = appList[i].iterator();
					for(int j = 0; j<appList[i].size(); j++) {
						tmp = itr.next();
						if(tmp.getAppointmentId() == app.getAppointmentId()) {
	
							if(tmp.getBill() != app.getBill()) {
								tmp.setBill(app.getBill());
							}
							if(tmp.getCarUnitId() != app.getCarUnitId()) {
								tmp.setCarUnitId(app.getCarUnitId());
							}
							
							if(!tmp.getJob().equals(app.getJob())) {
								tmp.setJob(app.getJob());
							}
							 return Response.status(201).build();
						}
					}
			  }
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      }
	 
//		Iterates through the lists until the appointment with the ID in the parameter is found, then deletes it. Otherwise, returns NOT FOUND	    
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deleteAppointment(@PathParam("id") long id){
	    	 Appointment tmp = new Appointment();
	    	  for(int i = 0; i<appList.length; i++) {
					Iterator<Appointment> itr = appList[i].iterator();
					for(int j = 0; j<appList[i].size(); j++) {
						tmp = itr.next();
						if(tmp.getAppointmentId() == id) {
							itr.remove();
							return Response.status(Response.Status.OK).build();
						}
					}
	    	  }
	    	       
	        return Response.status(Response.Status.NOT_FOUND).build();      
	    }  
//		Method for converting the days of the week into number, given that the parameter would be a day of the week using Strings
//		Defaults to 0, which represents Monday
	    private int findDay(String day) {
	    	switch(day) {
		    	case("Tuesday"):
		    		return 1;
		    	case("Wednesday"):
		    		return 2;
		    	case("Thursday"):
		    		return 3;
		    	case("Friday"):
		    		return 4;
	    	}
	    	return 0;
			
	    }
	   
}

