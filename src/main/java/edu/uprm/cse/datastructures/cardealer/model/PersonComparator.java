package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class PersonComparator<E> implements Comparator<E> {

	@Override
	public int compare(E o1, E o2) {
		Person person1 = (Person) o1;
		Person person2 = (Person) o2;
		if(person1.getFirstName().compareTo(person2.getFirstName()) == 0) { 
			
			return person1.getLastName().compareTo(person2.getLastName()); 
			
		} else
			
			return person1.getFirstName().compareTo(person2.getFirstName());
		
	}
}
