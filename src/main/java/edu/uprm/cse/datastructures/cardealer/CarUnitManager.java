package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.util.*;

@Path("/carunit")
public class CarUnitManager {	
// 	Empty constructor to access the manager in other classes when needed	
	public CarUnitManager() {
		
	}
//		Creates a sorted List using our created CSDLList which uses CarUnitComparator as a parameter and CarUnit as our data type
		private final static SortedList<CarUnit> cList = new CircularSortedDoublyLinkedList<CarUnit>(new CarUnitComparator<CarUnit>());                               

//		Gets all carUnits in the list

		@GET
		@Path("")
		@Produces(MediaType.APPLICATION_JSON)
		public CarUnit[] getAllCarUnits() {
			CarUnit[] tmp = new CarUnit[cList.size()];
			for(int i =0; i< cList.size(); i++) {
				tmp[i] = cList.get(i);
			}
			return tmp;
		}   
		
//		Gets carUnits by ID
//		Throws a NFException if ID does not exist
		  @GET
		  @Path("/{id}")
		  @Produces(MediaType.APPLICATION_JSON)
		  public CarUnit getCarUnit(@PathParam("id") long id){
			  for(int i = 0; i<cList.size();i++) {
				  if(cList.get(i).getCarUnitId() == id) {
					  return cList.get(i);
				  }
			  }
			  
		      throw new NotFoundException(new JsonError("Error", "CarUnit " + id + " not found"));
		    
		  }  
//			Adds a new carUnit to the sorted list
//			Before adding, it checks if the carId and personId given out in the parameter/new carUnit exists by using the CarManager and PersonManager getByID method
//			Even though the try/catch expression is not necessary, I keep it for code reading simplicity (the same exception would be called anyways).
//			Lastly, it checks that the ID or the carPlate + VIN of the new carUnit (since to my understanding,
//			these fields are unique per carUnit) isn't already on the list, giving a NOT ACCEPTABLE response status otherwise	  	

		  	@POST
		    @Path("/add")
		    @Produces(MediaType.APPLICATION_JSON)
		    public Response addCarUnit(CarUnit carUnit){
		  		CarManager car = new CarManager();
		  		PersonManager person = new PersonManager();
		  		CarUnit tmp = new CarUnit();
		  		try {
		  			car.getCar(carUnit.getCarId());		  		}
		  		catch(NotFoundException e) {
				      throw new NotFoundException(new JsonError("Error", "Car  " + carUnit.getCarId() + " not found"));
		  		}
		  		try {
		  			person.getPerson(carUnit.getPersonId()); 
		  		}
		  		catch(NotFoundException e) {
				      throw new NotFoundException(new JsonError("Error", "Person " + carUnit.getPersonId() + " not found"));
		  		}
				for(int i = 0; i<cList.size(); i++) {
					  tmp = cList.get(i);
					if(tmp.getCarUnitId() == carUnit.getCarUnitId() || (tmp.getCarPlate() == carUnit.getCarPlate() && tmp.getVin() == carUnit.getVin() ))
						return Response.status(Response.Status.NOT_ACCEPTABLE).build();     
				}
				  
		  		cList.add(carUnit);
		  		return Response.status(201).build();
		    }  
		  	

//			Uses the ID of the parameter carUnit and compares it within the existing ones in the list. 
//			When matched, removes the current carUnit on the list and adds the updated one to it
//			However, if ID in question isn't found on the list, it returns a response NOT FOUND
  	

		    @PUT
		    @Path("/{id}/update")
		    @Produces(MediaType.APPLICATION_JSON)
		    public Response updateCarUnit(CarUnit carUnit){
		    	for(int i = 0; i<cList.size(); i++) {
		    		if(carUnit.getCarUnitId() == cList.get(i).getCarUnitId()) {
		    			cList.remove(i);
		    			cList.add(carUnit);
		    	        return Response.status(Response.Status.OK).build();

		    		}
		    	}
		        return Response.status(Response.Status.NOT_FOUND).build();      
		      }
		    
//			Iterates through the lists until the carUnit with the ID in the parameter is found, then deletes it. Otherwise, returns NOT FOUND	    
		    @DELETE
		    @Path("/{id}/delete")
		    public Response deleteCarUnit(@PathParam("id") long id){
		    	for(int i = 0; i<cList.size();i++) {
		    		if(id == cList.get(i).getCarUnitId()) {
		    			cList.remove(i);
		    	        return Response.status(Response.Status.OK).build();

		    	}
		      }
		        return Response.status(Response.Status.NOT_FOUND).build();      
		    }  

}
